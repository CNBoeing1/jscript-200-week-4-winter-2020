/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = () => {
	let deck = new Array(); 
    let suits = ["hearts", "spades", "clubs", "diamonds"];
    let displayvals = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
    let vals = [2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10, 11];
    
    for( var i=0; i < suits.length; i++){
    	for( var j=0; j < vals.length; j++){
        	let card = { val: vals[j], displayVal: displayvals[j], suit: suits[i] };
    		deck.push(card);
        }
    }
    return deck;
}



// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);