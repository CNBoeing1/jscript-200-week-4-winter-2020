const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
//class CardPlayer {}; //TODO
class CardPlayer{
	constructor(name){
    	this.name = name;
        this.hand = new Array();
    }
    drawCard(){
    	let indx = Math.floor(Math.random() * 52);
        let deck = getDeck();
        this.hand.push(deck[indx]);
    };
};

// CREATE TWO NEW CardPlayers
//const dealer; // TODO
//const player; // TODO
const player = new CardPlayer("Player");
const dealer = new CardPlayer("Dealer");

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  	let isSoft = false, total = 0, val = 0, cnt = 0;
    let deck = getDeck();
    
    
    hand.forEach(card => {
    	if(card.displayVal != "A"){
        		val += card.val;
                cnt++;
        }
    });
    
    let aces = hand.length - cnt;
    if(aces == 1)
    	isSoft = true;
    if(val <= 10){
    	if( aces == 1){
        	val += 11;
        }
        else if( (aces > 1) && ((11 + (val + ((aces - 1) * 1))) <= 21)){
        	val += (11 + ((aces - 1) * 1));
        }
        else if (aces > 1){
        	val += (aces * 1);
        }
    }
    else{
    	val += (aces * 1);
    }

    let returnobject = { total: val, isSoft: isSoft};
    return returnobject;

}

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  	let obj = calcPoints(dealerHand);
    if( obj.total <= 16 ){
    	return true;
    }
    else if( obj.total === 17 && obj.isSoft ){
    	return true;
    }
    else return false;
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE
  let result = "";
  if(playerScore == dealerScore)
    result = `The player\'s score is ${playerScore}, the dealer\'s score is ${dealerScore}, and it is a tie.`;
  else if(playerScore > dealerScore)
    result = `The player\'s score is ${playerScore}, the dealer\'s score is ${dealerScore}, and the winner is player.`;
  else
    result = `The player\'s score is ${playerScore}, the dealer\'s score is ${dealerScore}, and the winner is dealer.`;
  return result;

}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);
  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);
  console.log(determineWinner(playerScore, dealerScore));
  return determineWinner(playerScore, dealerScore);
}
 console.log(startGame());